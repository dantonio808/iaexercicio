package segundaQuestao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;

public class Jogo implements Runnable{
	Queue<Tabuleiro> fila;
	ArrayList<Tabuleiro>avaliados;
	Tabuleiro tabInicial;
	
	
	public Jogo(Tabuleiro tabInicial) {
		super();
		this.tabInicial = tabInicial;
		this.fila = new LinkedList<Tabuleiro>();
		this.avaliados = new ArrayList<Tabuleiro>();
	}

	public Jogo() {
		super();
		this.fila = new LinkedList<Tabuleiro>();
		this.avaliados = new ArrayList<Tabuleiro>();
	}

	/**
	 * Metodo que para um tabuleiro,
	 * gera os filhos e ordena as duas listas
	 * @param tab
	 * @throws Exception 
	 */
	public void gerarFilhos(Tabuleiro tab) throws Exception{
		ArrayList<Tabuleiro> listaTemp = new ArrayList<Tabuleiro>();
		
		for(int i = 0;i< 8;i++){
			listaTemp.addAll(this.permutarPosicao(i, tab));
		}
		
//		listaTemp = this.permutarPosicao(7, tab);
//		
//		for(Tabuleiro t: listaTemp){
//			t.imprimirTabuleiro();
//		}
		
		this.checaAtaques(listaTemp);
		listaTemp.addAll(this.fila);
		
		this.fila = null;
		
		//ordenacao dos tabuleiros
		Collections.sort(listaTemp, new Comparator<Tabuleiro>(
				){
					@Override
					public int compare(Tabuleiro o1, Tabuleiro o2) {
						return o1.compareTo(o2);
					}
		});
		
		this.fila = new LinkedList<Tabuleiro>();
		this.fila.addAll(listaTemp);		
		
	}
	
	/**
	 * Metodo que recebe um tabuleiro e permuta
	 * aquela posicao com todas as outras,gerando os filhos e inserindo-os
	 * na fila temporaria antes de coloca-los de volta na fila original.
	 * @throws Exception 
	 */
	private ArrayList<Tabuleiro> permutarPosicao(int posicaoAlterada,Tabuleiro tab) throws Exception{
		ArrayList<Tabuleiro> temp =  new ArrayList<Tabuleiro>();
		int[] aux;
		int posicao;
		int troca;
		Tabuleiro tabTemp;
		
		posicao = posicaoAlterada;
		
		//permutando os elementos que estao apos a posicao.
		while(posicao < 6){
			aux = tab.copiaTabuleiro();
			troca = aux[posicao +1];
			aux[posicao + 1] =  aux[posicaoAlterada];
			aux[posicaoAlterada] = troca;
			
			tabTemp = new Tabuleiro(aux, tab);
			
			try {
				if (tab.getPai() == null || !tab.getPai().equals(tabTemp)) {
					temp.add(tabTemp);
				}
			} catch (NullPointerException n) {
				temp.add(tabTemp);
			}
			
			posicao++;
		}
		
		if (posicaoAlterada < 6) {
			//permutando a ultima posicao
			posicao++;
			aux = tab.copiaTabuleiro();
			troca = aux[posicao];
			aux[posicao] = aux[posicaoAlterada];
			aux[posicaoAlterada] = troca;

			tabTemp = new Tabuleiro(aux, tab);

			try {
				if (tab.getPai() == null || !tab.getPai().equals(tabTemp)) {
					temp.add(tabTemp);
				}
			} catch (NullPointerException n) {
				temp.add(tabTemp);
			}

			posicao = posicaoAlterada;
		}
		
		// permutando os elementos que estao antes da posicao.
		while(posicao > 1) {
			aux = tab.copiaTabuleiro();
			troca = aux[posicao - 1];
			aux[posicao - 1] = aux[posicaoAlterada];
			aux[posicaoAlterada] = troca;

			tabTemp = new Tabuleiro(aux, tab);

			try {
				if (tab.getPai() == null || !tab.getPai().equals(tabTemp)) {
					temp.add(tabTemp);
				}
			} catch (NullPointerException n) {
				temp.add(tabTemp);
			}

			posicao--;
		}
		
		// permutando a primeira posicao
		if (posicaoAlterada > 1) {
			posicao--;
			aux = tab.copiaTabuleiro();
			troca = aux[posicao];
			aux[posicao] = aux[posicaoAlterada];
			aux[posicaoAlterada] = troca;

			tabTemp = new Tabuleiro(aux, tab);
			
			try {
				if (tab.getPai() == null || !tab.getPai().equals(tabTemp)) {
					temp.add(tabTemp);
				}
			} catch (NullPointerException n) {
				temp.add(tabTemp);
			}
		}
		
		return temp;
		
	}
	
	
	
	/**
	 * Metodo que checa se um elemento
	 * ja foi avaliado anteriormente
	 * comparando seus tabuleiros.
	 * @param tab
	 * @return
	 */
	public boolean checaAvaliado(Tabuleiro tab){
		boolean jaFoi = false;
		
		for(Tabuleiro temp : this.avaliados){
			if(temp.equals(tab)){
				jaFoi = true;
				break;
			}
		}
		
		return jaFoi;
	}
	
	/**
	 *  Metodo que para cada elemento de uma lista
	 *  calcula o total de ataques que ocorreram
	 *  na configuracao do elmento.
	 * @param listaTab
	 */
	public void checaAtaques(ArrayList<Tabuleiro> listaTab){
		
		for(Tabuleiro t : listaTab){
			t.custo = this.buscaAtaquesPorPosicao(t.getTab());
		}
		
	}
	
	/**
	 * Metodo que para um vetor,trava cada posicao do vetor
	 * e checa a possibilidade de ataques nas diagonais 
	 * entre a posicao travada e as outras.
	 * 
	 * @param vetor do tabuleiro a ser avaliado.
	 * @return quantidade total de ataques.
	 */
	private int buscaAtaquesPorPosicao(int[] vetor){
		int pos = 0;
		int numAtaques = 0;
		
		//percorrer todas as posicoes do tabuleiro
		//checando o ataque da posicao
		//com cada elemento posterior a ela.
		while(pos < 8){
			
			//checando o limite.
			if((pos + 1)<= 7){
				//percorrendo os elementos posteriores a posicao Atual
				int segundaPos = pos;
				segundaPos++;
				
				while(segundaPos < 8){
					//exemplo: posicao1:[3][3] e posicao2[4][4]  3-4 == 3-4.
					if(Math.abs(pos - segundaPos) == Math.abs(vetor[pos] - vetor[segundaPos])){
						numAtaques++;
					}
					
					segundaPos++;	
				}
			}
			pos++;
		}
		
		return numAtaques;
	}
	
	private void imprimeSolucao(Tabuleiro solucao){
		Tabuleiro aux = solucao;
		int posX = 0;//posicoes de percorrer o vetor.
		int posY = 0;
		
		//fazer a impressao do tabuleiro com as rainhas.
		System.out.println("Solucao Obtida:");
		aux.imprimirTabuleiro();
		
		for(int i = 0; i < 8; i++){
			posY = solucao.getTab()[posX];
			System.out.print(" |");
			for(int j = 0; j < 8;j++){
				if(i == posX && j == posY ){
					System.out.print(" R |");
				}else{
					System.out.print(" _ |");
				}
				
			}
			
			System.out.println("  ");
			
			posX++;
		}
		
	}

	@Override
	public void run() {
		boolean achouSolucao = false;
		Tabuleiro atual = null;
		
		this.fila.add(this.tabInicial);
		
		do{
			atual = this.fila.remove();
			
			if(this.buscaAtaquesPorPosicao(atual.getTab()) == 0){
				achouSolucao = true;
			}else{
				if(!this.checaAvaliado(atual)){
					this.avaliados.add(atual);
					try {
						this.gerarFilhos(atual);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
		}while(!this.fila.isEmpty() && !achouSolucao);
		
		
		if(achouSolucao){
			this.imprimeSolucao(atual);
		}else{
			System.out.println("Nao foi possivel encontrar uma solucao para a configuracao informada!");
		}
		
	}
	

}
