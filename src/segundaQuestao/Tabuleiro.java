package segundaQuestao;

public class Tabuleiro {
	int[] tab;
	Tabuleiro pai;
	int custo;
	
	public Tabuleiro(int[] tab, Tabuleiro pai) throws Exception {
		super();
		
		this.validaTabuleiro(tab);
		this.tab = tab;
		this.pai = pai;
		this.custo = 0;
	}
	
	public int[] copiaTabuleiro(){
		int[] ret = new int[8];
		
		for(int i = 0; i < 8;i++){
			ret[i] = this.tab[i];
		}
		
		return ret;
	}
	
	public boolean equals(Tabuleiro tab){
		boolean ehIgual = true;
		int[] aux = tab.getTab();
		
		for(int i = 0;i < 8;i++){
			if(this.tab[i] != aux[i]){
				ehIgual = false;
				break;
			}
		}
		
		return ehIgual;
	}
	
	public void imprimirTabuleiro(){
		System.out.println("-------------------------");
		System.out.print("|");
		
		for(int i = 0; i < 8; i++){
			System.out.print(this.tab[i]+"|");
		}
		System.out.println(" ");
		
		System.out.println("--------------------------");
	}
	
	public int compareTo(Tabuleiro t2){
		int result = 0;
		
		if(this.custo > t2.getCusto()){
			result = 1;
		}else if(this.custo == t2.getCusto()){
			result = 0;
		}else{
			result = -1;
		}
		
		return result;
		
	}
	
	
	/**
	 *  Metodo que para um tabuleiro informado
	 *  faz a validacao impedindo que sejam
	 *  informados tabuleiros com elementos
	 *  de mesma linha/coluna ou elementos
	 *  que apontem para posicoes de fora
	 *  do tabuleiro.
	 * @throws Exception
	 */
	private void validaTabuleiro(int[] tabInformado) throws Exception{
		boolean isValido = true;
		
		for(int i = 0; i < 8 && isValido;i++){
			
			if(tabInformado[i] < 0 || tabInformado[i] > 7){
				isValido = false;
			}
			
			for(int j = i+1; j < 8 && isValido;j++){
				if(tabInformado[i] == tabInformado[j]){
					isValido = false;
				}
				
			}
		}
		
		if(!isValido){
			throw new Exception("O tabuleiro informado,n�o � valido!");
		}
	}

	public int[] getTab() {
		return tab;
	}

	public Tabuleiro getPai() {
		return pai;
	}

	public void setTab(int[] tab) {
		this.tab = tab;
	}

	public void setPai(Tabuleiro pai) {
		this.pai = pai;
	}


	public int getCusto() {
		return custo;
	}


	public void setCusto(int custo) {
		this.custo = custo;
	}
}
