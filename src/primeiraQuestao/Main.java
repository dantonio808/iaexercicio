package primeiraQuestao;

public class Main {
	
	public static void main(String[] args) {
		Tabuleiro tab;
		//int[][] aux = { { 0, 1, 3 }, { 4, 2, 5 }, { 7, 8, 6 } };
		int[][] aux = {{4,6,2},{8,1,3},{7,5,0}};
		tab = new Tabuleiro(aux, null);
		long tempoInicial = System.currentTimeMillis();
		Jogo jogo = new Jogo(tab);
		Thread t = new Thread(jogo);

		t.start();
		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		long tempoFinal = System.currentTimeMillis();
		System.out.println("TEMPO TOTAL: " +  (tempoFinal - tempoInicial) + "ms");

	}
	
}
