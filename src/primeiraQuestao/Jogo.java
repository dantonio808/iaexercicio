package primeiraQuestao;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Jogo implements Runnable {
	private Tabuleiro configuracaoInicial;
	private Queue<Tabuleiro> filaAvaliacao;
	private ArrayList<Tabuleiro> avaliados;
	private Tabuleiro objetivo;
	
	public Jogo(Tabuleiro configuracaoInicial) {
		this.configuracaoInicial = configuracaoInicial;
		this.filaAvaliacao = new LinkedList<Tabuleiro>();
		this.avaliados = new ArrayList<Tabuleiro>();
		int[][] aux = {{1,2,3},{4,5,6},{7,8,0}};
		this.objetivo = new Tabuleiro(aux, null);
		
	}

	@Override
	public void run() {
		this.filaAvaliacao.add(this.configuracaoInicial);
		boolean encontrouObj = false;
		Tabuleiro elem = null;
		
		do{
			elem = this.filaAvaliacao.remove();
			
			if(elem.equals(this.objetivo)){
				encontrouObj = true;
			}else{
				this.avaliados.add(elem);
				this.gerarFilhos(elem);
			}
		}while(!this.filaAvaliacao.isEmpty() && !encontrouObj);
		
		if(!encontrouObj){
			System.out.println("Nao ha solucao para o tabuleiro informado");
		}else{
			Stack<Tabuleiro> pilha = new Stack<Tabuleiro>();
			
			while(elem.getPai() != null){
				pilha.push(elem);
				elem = elem.getPai();
			}
			
			
			System.out.println("Quantidade de Passos:" + pilha.size());
			int i = 0;
			do{
				
				System.out.println("Passo: " + ++i);
				elem = pilha.pop();
				elem.imprimeTab();
			}while(!pilha.isEmpty());
		}
	}

	
	public void testeExpandirFilhos(Tabuleiro elem){
		
		this.gerarFilhos(elem);
		Tabuleiro temp;
		while(!this.filaAvaliacao.isEmpty()){
			temp = this.filaAvaliacao.remove();
			
			temp.imprimeTab();
		}
	}
	
	
	private void gerarFilhos(Tabuleiro elem){
		int[][] tabAux = elem.copiaMatriz();
		Tabuleiro temp;
		int[] coordPosZero = elem.getPosicaoZero();
		
//		System.out.println("Antes da Expansao:");
//		System.out.println("Fila = " + this.filaAvaliacao.size());
//		System.out.println("Lista Avaliados = " + this.avaliados.size());
//		
		//crescer para cima
		if(coordPosZero[0] >= 1 && coordPosZero[0] < 3 ){
			int valor = tabAux[coordPosZero[0] - 1][coordPosZero[1]];
			tabAux[coordPosZero[0]][coordPosZero[1]] = valor;
			tabAux[coordPosZero[0] - 1][coordPosZero[1]] = 0;
			
			temp = new Tabuleiro(tabAux, elem);
			
			if(!checaSeFoiAvaliado(temp)){
				this.filaAvaliacao.add(temp);
			}
		}
		
		//crescer pra esquerda
		tabAux = elem.copiaMatriz();
		if(coordPosZero[1] >= 1 && coordPosZero[1] < 3){
			int valor = tabAux[coordPosZero[0]][coordPosZero[1]-1];
			tabAux[coordPosZero[0]][coordPosZero[1]] = valor;
			tabAux[coordPosZero[0]][coordPosZero[1] - 1] = 0;
			temp = new Tabuleiro(tabAux, elem);
			
			if(!checaSeFoiAvaliado(temp)){
				this.filaAvaliacao.add(temp);
			}
		}

		tabAux = elem.copiaMatriz();
		//crescer pra direita
		if(coordPosZero[1] <= 1 && coordPosZero[1] < 3){
			int valor = tabAux[coordPosZero[0]][coordPosZero[1]+ 1];
			tabAux[coordPosZero[0]][coordPosZero[1]] = valor;
			tabAux[coordPosZero[0]][coordPosZero[1] + 1] = 0;
			temp = new Tabuleiro(tabAux, elem);
			
			if(!checaSeFoiAvaliado(temp)){
				this.filaAvaliacao.add(temp);
			}
		}
		
		//crescer pra baixo		
		tabAux = elem.copiaMatriz();
		if(coordPosZero[0] <= 1 && coordPosZero[0] < 3){
			int valor = tabAux[coordPosZero[0] + 1][coordPosZero[1]];
			tabAux[coordPosZero[0]][coordPosZero[1]] = valor;
			tabAux[coordPosZero[0] + 1][coordPosZero[1]] = 0;
			temp = new Tabuleiro(tabAux, elem);
			
			if(!checaSeFoiAvaliado(temp)){
				this.filaAvaliacao.add(temp);
			}
		}
		
//		System.out.println("Depois da Expansao:");
//		System.out.println("Fila = " + this.filaAvaliacao.size());
//		System.out.println("Lista Avaliados = " + this.avaliados.size());
	}
	
	private boolean checaSeFoiAvaliado(Tabuleiro elem){
		boolean avaliado = false;
		
		for(Tabuleiro aux : this.avaliados){
			if(aux.equals(elem)){
				avaliado = true;
				break;
			}
		}
		
		return avaliado;
	}
	public Tabuleiro getConfiguracaoInicial() {
		return this.configuracaoInicial;
	}

	public void setConfiguracaoInicial(Tabuleiro configuracaoInicial) {
		this.configuracaoInicial = configuracaoInicial;
	}

}
