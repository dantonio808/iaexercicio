package primeiraQuestao;

public class Tabuleiro {
	private int[][] tab;
	private Tabuleiro pai;
	
	public Tabuleiro(int[][] tab, Tabuleiro pai) {
		super();
		this.tab = tab;
		this.pai = pai;
	}
	
	/**
	 * Metodo que percorre o tabuleiro
	 * informando as coordenadas da posicao 0.
	 * para isso ele usa um vetor de inteiro
	 * onde a posicao [0] � a linha
	 * e a posicao[1] � a coluna.
	 * @return
	 */
	public int[] getPosicaoZero(){
		int[] coord = new int[2];
		boolean achou = false;
		int i = 0;
		int j = 0;
		
		for(i = 0; i < 3 && !achou;i++){
			for(j = 0; j < 3 && !achou;j++){
				if(this.tab[i][j] == 0){
					achou = true;
					coord[0] = i;
					coord[1] = j;
				}
			}
		}
		
		
		
		return coord;
	}
	
	public boolean equals(Tabuleiro tab){
		boolean ehIgual = true;
		int[][] aux = tab.getTab();
		
		for(int i = 0; i < 3 && ehIgual; i++){
			for(int j = 0; j < 3 && ehIgual; j++){
					if(this.tab[i][j] != aux[i][j]){
						ehIgual = false; 
					}
			}
		}
		return ehIgual;
	} 
	
	public int[][] copiaMatriz(){
		int[][] copia = new int[3][3];
		
		for(int i = 0;i < 3;i++){
			for(int j = 0; j < 3;j++){
				copia[i][j] = this.tab[i][j];
			}
		}	
		return copia;
	}
	
	
	public void imprimeTab(){
		System.out.println("---------------------------");
		for(int i = 0;i < 3;i++){
			System.out.print("|");
			for(int j = 0; j < 3;j++){
				System.out.print(this.tab[i][j] + " |");
			}
			System.out.println(" ");
		}
		
		System.out.println("---------------------------");
	}
	
	//Getters e Setters.
	public int[][] getTab() {
		return this.tab;
	}
	public Tabuleiro getPai() {
		return this.pai;
	}
	public void setTab(int[][] tab) {
		this.tab = tab;
	}
	public void setPai(Tabuleiro pai) {
		this.pai = pai;
	}
	
}
