package terceiraQuestao;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Stack;

/**
 * @author Diego
 * Classe que faz os testes de combina��es de caminhos entre todas as estacoes poss�veis.
 */
public class TestesAuto {
	final static String caminhoDefault= "C:\\Users\\Diego\\Desktop\\";
	final static String nomeDefault = "Solucao Metro.txt";
	final static int inicio = 0;
	final static int max = 14;
	final static StringBuffer saidaArquivo = new StringBuffer();
	
	public static void main(String[] args) throws IOException {
		int i,j;
		No origem,destino;
		Thread t;
		Busca b;
		
		
		for(i = inicio ; i < (max-1);i++){
			origem = new No(i, null);
			for(j = i + 1; j < max;j++){
				destino = new No(j, null);
				b = new Busca(origem, destino);
				b.setSaidaArquivo(true);
				t = new Thread(b);
				
				t.start();
				
				try {
					t.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if(b.getNoSolucao() != null){
					imprimimeSolucaoArquivo(origem, destino, b.getNoSolucao());
				}
			}
		}
		
		FileWriter escritor = new FileWriter(caminhoDefault + nomeDefault);
		PrintWriter printer = new PrintWriter(escritor);
		printer.write(saidaArquivo.toString());

		printer.close();
		escritor.close();
		
	}
	
	
	
	private static void imprimimeSolucaoArquivo(No origem,No destino,No solucao){
		Stack<No> pilha = new Stack<No>();
		No temp = solucao;
		
		while(temp != null){
			pilha.add(temp);
			temp = temp.getPai();
		}
		
		saidaArquivo.append("*=====================================================*" + "\n");
		saidaArquivo.append("   SOLUC�O  E" + (origem.getEstacaoRepresentada() + 1) + " -> E"+ (destino.getEstacaoRepresentada() + 1) + "\n");
		saidaArquivo.append("   Total de estacoes Percorridas: "  + (pilha.size() -1)+ "\n");
		saidaArquivo.append("   ");
		do{
			temp = pilha.pop();
			saidaArquivo.append("E" + (temp.getEstacaoRepresentada() + 1));
			
			if(!pilha.isEmpty()){
				saidaArquivo.append(" > ");
			}
			
		}while(!pilha.isEmpty());
		saidaArquivo.append("\n*=====================================================*" + "\n");
	}

}
