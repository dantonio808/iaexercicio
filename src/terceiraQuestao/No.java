package terceiraQuestao;

public class No {
	int estacaoRepresentada;//representa a estacao
	No pai;//representa a estacao anterior a essa.
	int custo;//guarda o valor percorrido entre estacoes + o custo em linha reta.
	int distanciaPercorrida;//guarda o valor percorrido entre estacoes.
	
	public No(int estacaoRepresentada, No pai) {
		super();
		this.estacaoRepresentada = estacaoRepresentada;//corrigindo a posicao no vetor
		this.pai = pai;
	}

	
	public boolean equals(No outro){
		boolean isEqual = false;
		
		if(this.estacaoRepresentada == outro.getEstacaoRepresentada()){
			isEqual = true;
		}
		
		return isEqual;
	}
	
	public int compareTo(No outro){
		int valor = 0;
		
		valor = this.custo - outro.getCusto();
		
		return valor;
	}

	public int getEstacaoRepresentada() {
		return estacaoRepresentada;
	}

	public void setEstacaoRepresentada(int estacaoRepresentada) {
		this.estacaoRepresentada = estacaoRepresentada;
	}
	
	public No getPai() {
		return pai;
	}

	public void setPai(No pai) {
		this.pai = pai;
	}

	public int getCusto() {
		return custo;
	}

	public void setCusto(int custo) {
		this.custo = custo;
	}

	public int getDistanciaPercorrida() {
		return distanciaPercorrida;
	}


	public void setDistanciaPercorrida(int distanciaPercorrida) {
		this.distanciaPercorrida = distanciaPercorrida;
	}

	
}
