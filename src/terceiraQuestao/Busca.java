package terceiraQuestao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Busca implements Runnable {
	private No estadoInicial;
	private No estadoFinal;
	private Queue<No> fila;
	private ArrayList<No> avaliados;
	private final int TAM = 14;
	private No noSolucao;//solucao para impressao em arquivo
	private boolean saidaArquivo;//boolean indicando a saida do programa.
	
	
	public Busca(No estadoInicial,No estadoFinal) {
		super();
		this.estadoInicial = estadoInicial;
		this.estadoFinal = estadoFinal;
		this.fila = new LinkedList<No>();
		this.avaliados = new ArrayList<No>();
		this.saidaArquivo = false;
	}
	
	
	public void gerarFilhos(No estadoAtual){
		ArrayList<No> temp = new ArrayList<No>();
		No aux = null;
		
		for(int i = 0; i < TAM;i++){
			if(InformacaoApoio.testaCaminhoEstacoes(estadoAtual.getEstacaoRepresentada(), i) > 0){
				aux = new No(i, estadoAtual);
				aux.setDistanciaPercorrida(estadoAtual.getDistanciaPercorrida() 
						+ InformacaoApoio.testaCaminhoEstacoes(estadoAtual.getEstacaoRepresentada(),i));
				
				aux.setCusto(aux.getDistanciaPercorrida()+
						InformacaoApoio.getDistanciaEntreEstacoes(this.estadoFinal.getEstacaoRepresentada(), i));
				try {
					if (!aux.equals(estadoAtual) && 
						!estadoAtual.getPai().equals(aux)) {
						temp.add(aux);
					}
				} catch (NullPointerException e) {
					temp.add(aux);
				}
			}
		}
		
		temp.addAll(this.fila);
		
		Collections.sort(temp, new Comparator<No>(
				){
					@Override
					public int compare(No o1, No o2) {
						return o1.compareTo(o2);
					}
		});
		
		this.fila = new LinkedList<No>();
		this.fila.addAll(temp);
	}


	@Override
	public void run() {
		No noAtual = null;
		No solucao = null;
		
		this.fila.add(this.estadoInicial);
		do{
			noAtual = this.fila.remove();
			
			if(noAtual.equals(this.estadoFinal)){
				solucao = noAtual;
				break;
			}else{
				if(!this.checaAvaliados(noAtual)){
					this.gerarFilhos(noAtual);
					this.avaliados.add(noAtual);
				}
			}
		}while(!this.fila.isEmpty());
		
		if(solucao != null){
			if(!this.saidaArquivo){
				this.imprimirSolucao(solucao);
			}else{
				this.noSolucao = solucao;
			}
		}else{
			System.out.println("Houston,we have a problem");
		}
		
	}
	
	private void imprimirSolucao(No solucao){
		Stack<No> pilha = new Stack<No>();
		No temp = solucao;
		
		while(temp != null){
			pilha.add(temp);
			temp = temp.getPai();
		}
		
		System.out.println("SOLUC�O ");
		System.out.println("Total de estacoes Percorridas: "  + pilha.size());
		
		do{
			temp = pilha.pop();
			System.out.print("E" + (temp.getEstacaoRepresentada() + 1));
			
			if(!pilha.isEmpty()){
				System.out.print(" > ");
			}
			
		}while(!pilha.isEmpty());
		
		
	}
	
	
	public No retornaSolucao(){
		return null;
	}
	
	private boolean checaAvaliados(No no){
		boolean avaliado = false;
		
		for (No aux : this.avaliados) {
			if(no.equals(aux)){
				avaliado = true;
				break;
			}
		}
		
		return avaliado;
	}

	public No getEstadoInicial() {
		return estadoInicial;
	}

	public void setEstadoInicial(No estadoInicial) {
		this.estadoInicial = estadoInicial;
	}

	public Queue<No> getFila() {
		return fila;
	}

	public void setFila(Queue<No> fila) {
		this.fila = fila;
	}

	public ArrayList<No> getAvaliados() {
		return avaliados;
	}

	public void setAvaliados(ArrayList<No> avaliados) {
		this.avaliados = avaliados;
	}


	public No getEstadoFinal() {
		return estadoFinal;
	}


	public void setEstadoFinal(No estadoFinal) {
		this.estadoFinal = estadoFinal;
	}


	public No getNoSolucao() {
		return noSolucao;
	}


	public boolean isSaidaArquivo() {
		return saidaArquivo;
	}


	public void setNoSolucao(No noSolucao) {
		this.noSolucao = noSolucao;
	}


	public void setSaidaArquivo(boolean saidaArquivo) {
		this.saidaArquivo = saidaArquivo;
	}
	
}
