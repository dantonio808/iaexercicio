package terceiraQuestao;

public class Main {
	/**
	 * S� ser�o aceitos valores entre 0(E1) - 13(E14).
	 * @throws InterruptedException 
	 * */
	public static void main(String[] args) throws InterruptedException {
		final int estacaoOrigem = 6;
		final int estacaoDestino = 10;
		No no = new No(estacaoOrigem,null);
		No noDestino = new No(estacaoDestino,null);
		Busca b = new Busca(no,noDestino);
		Thread t1 = new Thread(b);
		
		t1.start();
		t1.join();
		
	}
}
