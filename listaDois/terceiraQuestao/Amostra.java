package terceiraQuestao;

public class Amostra {
	private double x;
	private double y;
	private int centroideEsperado;
	private int centroideObtido;
	
	public Amostra(double c1, double c2, int centroideEsperado) {
		super();
		this.x = c1;
		this.y = c2;
		this.centroideEsperado = centroideEsperado;
	}
	
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public int getCentroideEsperado() {
		return centroideEsperado;
	}
	public void setCentroideEsperado(int centroideEsperado) {
		this.centroideEsperado = centroideEsperado;
	}
	public int getCentroideObtido() {
		return centroideObtido;
	}
	public void setCentroideObtido(int centroideObtido) {
		this.centroideObtido = centroideObtido;
	}

}
