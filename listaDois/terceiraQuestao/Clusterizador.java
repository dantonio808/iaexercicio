package terceiraQuestao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Clusterizador {
	double[][] matrizDistancia;
	ArrayList<Centroide> centroides;
	ArrayList<Amostra> base;
	
	public Clusterizador() {
		super();
		
		
		try {
			this.lerAmostras();
			this.inicializarCentroides();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	private void lerAmostras() throws FileNotFoundException{
		String caminhoCasa = "C:\\Users\\Diego\\workspace\\ExercicioIA\\dados\\kmeans.txt";
	//	String caminhoNTI = "//home//diego.ferreira//workspace//bitBucket//iaexercicio//dados//kmeans.txt";
		File arquivo = new File(caminhoCasa);
		Scanner scan = new Scanner(arquivo);
		String aux;
		String[] valores;
		Amostra temp;
		
		this.base = new ArrayList<Amostra>(); 
		
		while(scan.hasNext()){
			aux = scan.next();
			
			valores = aux.split(";");
			
			temp = new Amostra(Double.parseDouble(valores[0]),Double.parseDouble(valores[1]) , Integer.parseInt(valores[2]));
			
			this.base.add(temp);
		}
		
		scan.close();
		
	}
	
	
	private void inicializarCentroides(){
		Centroide cent;
		
		this.centroides = new ArrayList<Centroide>();
		
		//Cria��o dos centr�ides.
		for(int i = 0; i < Constantes.numCentroides;i++){
			cent = new Centroide();
			cent.setNumCentroide(i + 1);
			this.centroides.add(cent);
		}
		
		Amostra temp;
		int valor  = this.base.size()/Constantes.numCentroides;
		
		//atribuindo centr�ides aleat�rios aos dados.
		for(int i = 0,cont = 0; i < Constantes.numCentroides;i++){
			
			do{
				temp = this.base.get(cont);
				temp.setCentroideObtido(i + 1);
				cont++;
			}while(cont < ((i + 1) * valor) && cont < this.base.size());
				
		}
		
	}
	
	private double calculaDistancia(Amostra amostraAtual,Centroide centroide){
		double distancia = 0.0;
		
		//formula de manhattan.
		distancia += Math.abs(amostraAtual.getX() - centroide.getX());
		
		distancia += Math.abs(amostraAtual.getY() - centroide.getY());
		
		return distancia;
	}

	
	private boolean agrupaAmostra(Amostra atual,int posMatriz){
		boolean houveTroca = true;
		int centroideEscolhido = -1;
		int teste = Constantes.numCentroides;
		
		if(teste == 2){
			if(this.matrizDistancia[posMatriz][0] < this.matrizDistancia[posMatriz][1]){
				centroideEscolhido = 1;
			}else{
				centroideEscolhido = 2;
			}
		}else{
			if(this.matrizDistancia[posMatriz][0] < this.matrizDistancia[posMatriz][1]
				&& this.matrizDistancia[posMatriz][0] < this.matrizDistancia[posMatriz][2]){
				centroideEscolhido = 1;
			}else if(this.matrizDistancia[posMatriz][1] < this.matrizDistancia[posMatriz][0]
				&& this.matrizDistancia[posMatriz][1] < this.matrizDistancia[posMatriz][2]){
				centroideEscolhido = 2;
			}else{
				centroideEscolhido = 3;
			}
			
		}
		
		if(centroideEscolhido == atual.getCentroideObtido()){
			houveTroca =  false;
		}else{
			atual.setCentroideObtido(centroideEscolhido);
		}
		
		return houveTroca;
	}
	
	
	private void atualizarCentroides(){
		double novoX,novoY;
		
		for (Centroide centro : this.centroides) {
			novoX = this.retiraMediaXCentroide(centro.getNumCentroide());
			novoY = this.retiraMediaYCentroide(centro.getNumCentroide());
			
			centro.setX(novoX);
			centro.setY(novoY);
		}
		
	}
	
	
	
	private double retiraMediaXCentroide(int centroide){
		double novaMedia = 0.0;
		int qtdade = 0;
		
		for (Amostra amostra : this.base) {
			
			if(amostra.getCentroideObtido() == centroide){
				qtdade++;
				novaMedia += amostra.getX();
			}
			
		}
		
		novaMedia = novaMedia/qtdade;
		
		return novaMedia;
	}
	
	
	private double retiraMediaYCentroide(int centroide){
		double novaMedia = 0.0;
		int qtdade = 0;
		
		for(Amostra amostra : this.base){
			if(amostra.getCentroideObtido() == centroide){
				qtdade++;
				novaMedia += amostra.getY();
			}
		}
		
		novaMedia = novaMedia/qtdade;
		return novaMedia;
	}
	
	
	private void saidaPraArquivo() throws FileNotFoundException{
		StringBuffer sb = new StringBuffer();
		String caminhoArquivo = "C:\\Users\\Diego\\Desktop\\";
		String nomeArquivo = "saidaKMeans.txt";
		PrintWriter escritor = new PrintWriter(caminhoArquivo+nomeArquivo);
		int y = 1;
		sb.append("Centroides:\n");
		for (Centroide centro : this.centroides) {
			sb.append("C" + y + "(" + centro.getX() + "," + centro.getY() + ")\n");
			y++;
		}
		
		
		sb.append("Amostras Analisadas\n");
		int i = 0;
		
		for (Amostra am : this.base) {
			sb.append("Amostra " + i + " : " + am.getX() + " | " + am.getY() + " | " + am.getCentroideEsperado()
					+ " | " + am.getCentroideObtido() + " \n");
			i++;
		}
		
		escritor.write(sb.toString());
		escritor.close();
	}
	
	
	public void classificacaoKmeans(){
		boolean troca = true;
		int cont = 0;
		
		do{
			this.matrizDistancia = new double[this.base.size()][Constantes.numCentroides];
			
			this.atualizarCentroides();
			
			for (Amostra amostraAtual : this.base ) {
				int i = 0;
				int j = 0;
				for(Centroide centroideAtual : this.centroides){
					this.matrizDistancia[i][j] = this.calculaDistancia(amostraAtual, centroideAtual);
					
					j++;
				}
				
				troca = this.agrupaAmostra(amostraAtual,i);
				
				if(troca){
					cont = 0;
				}
				
				i++;
				cont++;
				
				if(cont >= this.base.size()){
					break;
				}
				
			}
		
			if(cont < this.base.size()){
				this.atualizarCentroides();
			}else{
				break;
			}
			
		}while(true);
		
		try {
			this.saidaPraArquivo();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
