package terceiraQuestao;

public class Centroide {
	double x;
	double y;
	int numCentroide;
	
	public Centroide(double x, double y, int numCentroide) {
		super();
		this.x = x;
		this.y = y;
		this.numCentroide = numCentroide;
	}

	public Centroide() {
		super();
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public int getNumCentroide() {
		return numCentroide;
	}

	public void setNumCentroide(int numCentroide) {
		this.numCentroide = numCentroide;
	}
	
}
