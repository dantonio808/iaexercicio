package primeiraQuestao;

public class Individuo {
	int[] tabuleiro;
	int custo;
	Individuo pai;
	boolean mutacao = false;
	
	// Construtor Default
	public Individuo() {
		super();
	}
	
	public Individuo(int[] tabuleiro, Individuo pai) {
		super();
		this.tabuleiro = tabuleiro;
		this.pai = pai;
	}
	
	
	public boolean equals(Individuo outro){
		boolean isEqual = true;
		int[] aux = outro.getTabuleiro();
		
		for(int i = 0; i < 8;i++){
			if(aux[i] != this.tabuleiro[i]){
				isEqual = false;
				break;
			}
		}
		
		return isEqual;
	}
	

	//Getters e Setters.
	public int[] getTabuleiro() {
		return tabuleiro;
	}

	public boolean isMutacao() {
		return mutacao;
	}

	public void setMutacao(boolean mutacao) {
		this.mutacao = mutacao;
	}

	public int getCusto() {
		return custo;
	}

	public Individuo getPai() {
		return pai;
	}

	public void setTabuleiro(int[] tabuleiro) {
		this.tabuleiro = tabuleiro;
	}

	public void setCusto(int custo) {
		this.custo = custo;
	}

	public void setPai(Individuo pai) {
		this.pai = pai;
	}

}
