package primeiraQuestao;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Random;

/**
 * Classe responsavel pela aplicacao do
 * algoritmo genetico.
 * @author Diego
 *
 */
public class Genetica implements Runnable {
	final int maxIteracoes = 10000;//numero maximo de iteracoes q o algoritmo vai executar.
	final int tamVetor = 8;//tamanho do vetor de gene.
	final int tamCromossomo = 3;//tamanho de 1 cromossomo.
	final int valorRandMutacao = 40;//valor random,para calculo da possibilidade de mutacao.
	ArrayList<Individuo> populacao;//populacao utilizada no problema inteiro.	
	
	public Genetica() {
		super();
		this.populacao = new ArrayList<Individuo>();
	}
	
	@Override
	public void run() {
		boolean solucao = false;
		this.gerarPopulacaoInicial();
		Individuo sol = null;
		
		int numIt = 0;
		
		do{
			sol = this.fitness();
			if(sol != null){
				solucao = true;
			}else{
				this.gerarFilhos(this.populacao);
			}
			numIt++;
		}while(!solucao && numIt < maxIteracoes);
		
		
		if(!solucao){
				System.out.println("Nao foi possivel encontrar solucao com a quantidade de iteracoes.");
				System.out.println("populacao Atual:");
				this.imprimimePopulacao(this.populacao);
		}else{
			NumberFormat formato = DecimalFormat.getInstance();
			System.out.println("Solucao encontrada apos: " + formato.format(numIt) + " Iteracoes");
			this.imprimeSolucao(sol);
		}
		
	}
	
	
	/**
	 *  Metodo que checa a aptidao da populacao
	 *  checando se ha alguem com o custo 0.
	 *  Caso encontre,retorna esse elemento.
	 *  Se n�o retorna null.
	 * @return
	 */
	public Individuo fitness(){
		Individuo ideal = null;
		for (Individuo individuo : this.populacao) {
			if(individuo.getCusto() == 0){
				ideal = individuo;
			}
		}
		
		return ideal;
	}
	
	/**
	 *  Metodo que pega uma cole��o de
	 *  prov�veis pais e gera os filhos
	 *  aplicando sorteios entre eles.
	 * @param pais
	 */
	private void gerarFilhos(ArrayList<Individuo> pais){
		ArrayList<Individuo> filhos = new ArrayList<Individuo>();
		Random rand = new Random();
		int numSorteio = 0;
		Individuo pai = null;
		Individuo mae = null;
		
		do{
			//fazer Sorteio
			pai = this.sorteio();
			int cont = 0;
			
			do{
				mae = this.sorteio();
				if(mae.equals(pai)){
					cont++;
				}
			
				if(cont > 4){
					break;
				}
			}while(mae.equals(pai));
			
			numSorteio++;
			
			//gerar os filhos
			filhos.addAll(this.crossOver(pai, mae));
			
		}while(numSorteio < 2);
		
		//aplicar mutacao
		numSorteio = rand.nextInt(valorRandMutacao);
		
		if(numSorteio < filhos.size()){
			Individuo temp = filhos.get(numSorteio);
			filhos.remove(numSorteio);
			this.mutacao(temp);
			filhos.add(temp);
		}
		
		//drop na populacao e coloca os filhos na nova populacao.
		this.populacao = new ArrayList<Individuo>();
		this.populacao.addAll(filhos);
		
	}
	
	/** Metodo responsavel por selecionar
	 *  os individuos para o crossover.
	 *  Seleciona 2 individuos aleatoriamente
	 *   e retorna o melhor entre eles.
	 * @return Individuo selecionado para o crossover.
	 */
	private Individuo sorteio(){
		Individuo individuoSelecionado = null;
		Individuo e1,e2;
		Random rand = new Random();
		
		e1 = this.populacao.get(rand.nextInt(this.populacao.size()));
		e2 = this.populacao.get(rand.nextInt(this.populacao.size()));
		
		if(e1.getCusto() < e2.getCusto()){
			individuoSelecionado = e1;
		}else{
			individuoSelecionado = e2;
		}
		return individuoSelecionado;
	}
	
	
	/**
	 * Metodo que faz o crossover entre dois pais.
	 * Ele separa os genes dos filhos(tabuleiro)
	 * e faz o crossover dos cromossomos(posicoes do vetor) do pai
	 * com a m�e num ponto de corte definido aleatoriamente.
	 * @param end1 - pai
	 * @param end2 - m�e
	 * @return {@link ArrayList} com os filhos.
	 */
	private ArrayList<Individuo> crossOver(Individuo end1,Individuo end2){
		int[] genePai,geneMae;
		Random rand = new Random();
		int pontoCorte = rand.nextInt(tamVetor * tamCromossomo);
		int aux;
		ArrayList<Individuo> filhos = new ArrayList<Individuo>();
		Individuo temp = null;
		
		genePai = this.converterCromossomo(end1.getTabuleiro());
		
		geneMae = this.converterCromossomo(end2.getTabuleiro());
		
		for(int i = pontoCorte;i < (tamVetor * tamCromossomo);i++){
			aux = genePai[i];
			genePai[i] = geneMae[i];
			geneMae[i] = aux;
		}
		
		temp = new Individuo(this.desconverteCromossomo(genePai),null);
		filhos.add(temp);
		
		temp = new Individuo(this.desconverteCromossomo(geneMae),null);
		filhos.add(temp);
		
		this.calculaAtaquesRainhas(filhos);
		
		return filhos;
	}
	
	
	/**
	 * 	Metodo que realiza o processo de mutacao num filho.
	 * 	Ele recebe o filho, e pega um cromossomo aleatorio(valor de posicao)
	 *  e altera um dos bits de sua codificacao.
	 * @param filho
	 */
	private void mutacao(Individuo filho){
		int[] tab = filho.getTabuleiro();
		int[] cromossomo;
		int numRand;
		int numRand2;
		Random rand = new Random();
		
		//seleciona 1 elemento.
		numRand = rand.nextInt(tamVetor);
		cromossomo = this.deInteiroPraBit(tab[numRand]);
		
		//seleciona posicao no elemento.
		numRand2 = rand.nextInt(3);
		
		//troca o bit
		if(cromossomo[numRand2] == 0){
			cromossomo[numRand2] = 1;
		}else{
			cromossomo[numRand2] = 0;
		}
		
		tab[numRand] = this.deBitPraInteiro(cromossomo);
		
		filho.setMutacao(true);
		filho.setTabuleiro(tab);
		this.calculaAtaquePorPosicao(filho);
		
	}
	
	
	/**
	 * Metodo que cria a primeira populacao no problema.
	 * para isso cria 4 individuos,com tabuleiros totalmente
	 * aleatorios.
	 */
	private void gerarPopulacaoInicial(){
		Individuo temp = null;
		int[] vetorAux = null;
		Random rand = new Random();
		
		//cria 2 individuos com tabuleiros aleatorios.
		for(int i = 0; i < 2; i++){
			vetorAux = new int[tamVetor];
			//preenchendo as posicoes randomicamentes.
			for(int j = 0; j < tamVetor;j++){
				vetorAux[j] = rand.nextInt(tamVetor);
			}
			
			temp = new Individuo(vetorAux,null);
			this.populacao.add(temp);
		}
		
		this.inverterBitPopInicial();
		this.calculaAtaquesRainhas(this.populacao);
		
	}

	/**	Metodo que imprime o tabuleiro como vetor
	 *  com algumas informacoes como custo e se 
	 *  sofreu mutacao.
	 * @param ind
	 */
	private void imprimirTab(Individuo ind){
		int[] tabAux = ind.getTabuleiro();
		
		System.out.println("Custo:" +ind.getCusto());
		System.out.println("Sofreu Mutacao: " + ind.isMutacao());
		System.out.print("[");
		for(int i = 0;i < tamVetor; i++){
			System.out.print(" " + tabAux[i]);
			
			if(i < (tamVetor- 1)){
				System.out.print(",");
			}
			
		}
		System.out.println("]");
		
	}
	 
	/** Metodo que calcula os ataques em tabuleiros
	 * de uma lista.
	 * @param lista
	 */
	private void calculaAtaquesRainhas(ArrayList<Individuo> lista){
		
		for (Individuo individuo : lista) {
			this.calculaAtaquePorPosicao(individuo);
		}
		
	}
	
	
	/**
	 * Metodo que para um individuo,calcula todos os ataques
	 * de seu tabuleiro.
	 * @param ind
	 */
	private void calculaAtaquePorPosicao(Individuo ind){
		int pos = 0;
		int numAtaques = 0;
		int vetor[] = ind.getTabuleiro();
		
		//percorrer todas as posicoes do tabuleiro
		//checando o ataque da posicao
		//com cada elemento posterior a ela.
		while(pos < 8){
			
			//checando o limite.
			if((pos + 1)<= 7){
				//percorrendo os elementos posteriores a posicao Atual
				int segundaPos = pos;
				segundaPos++;
				//checa atques na diagonal.
				while(segundaPos < 8){
					//exemplo: posicao1:[3][3] e posicao2[4][4]  3-4 == 3-4.
					if(Math.abs(pos - segundaPos) == Math.abs(vetor[pos] - vetor[segundaPos])){
						numAtaques++;
					}
					
					segundaPos++;	
				}
			}
			pos++;
		}
		
		
		//calculando os ataques em coluna.	
		for(int i = 0; i < tamVetor;i++){
			for(int j = i + 1; j < tamVetor;j++){
				if(vetor[i] == vetor[j]){
					numAtaques++;
				}
			}
		}
		
		
		ind.custo = numAtaques;
	}
	
	
	/** Metodo de conversao de bits,representado por int[] para
	 *  valores inteiros.
	 * @param valorEmBit valor a ser convertido.
	 * @return valor do bit em inteiro.
	 */
	private int deBitPraInteiro(int[] valorEmBit){
		int valor = 0;
		for(int i = 0,pos= 2; i < 3;i++,pos--){
			if(valorEmBit[i] > 0){
				valor+= Math.pow(2, pos);
			}
		}
		
		return valor;
	}
	
	/**
	 *  Metodo que para um valor inteiro
	 *  faz a convers�o para bit,aqui
	 *  representado por int[].
	 * @param valorEmInteiro
	 * @return valor em bit convertido.
	 */
	private int[] deInteiroPraBit(int valorEmInteiro){
		int[] valorBit = new int[3];
		
		for(int i = 0, pos = 2; i < 3;i++,pos--){
			if(valorEmInteiro%2 == 0){
				valorBit[pos] = 0;
			}else{
				valorBit[pos] = 1;
			}
			valorEmInteiro/=2;
		}
		
		return valorBit;
	}
	
	/**
	 * Metodo que imprime a solucao mostrando
	 * o tabuleiro inteiro.
	 * @param solucao
	 */
	private void imprimeSolucao(Individuo solucao){
		Individuo aux = solucao;
		int posX = 0;//posicoes de percorrer o vetor.
		int posY = 0;
		
		//fazer a impressao do tabuleiro com as rainhas.
		System.out.println("Solucao Obtida:");
		this.imprimirTab(aux);
		
		for(int i = 0; i < 8; i++){
			posY = solucao.getTabuleiro()[posX];
			System.out.print(" |");
			for(int j = 0; j < 8;j++){
				if(i == posX && j == posY ){
					System.out.print(" R |");
				}else{
					System.out.print(" _ |");
				}
				
			}
			
			System.out.println("  ");
			
			posX++;
		}
		
	}
	
	/** 
	 * Metodo que imprimime uma populacao
	 * inteira usando o metodo de impressao
	 * normal de individuo.
	 * @param pop
	 */
	private void imprimimePopulacao(ArrayList<Individuo> pop){
		int i = 0;
		for (Individuo individuo : pop) {
			System.out.println("Individuo: " + (i++));
			this.imprimirTab(individuo);
		}
	}
	
	/** Metodo que converte um gene para sua 
	 * representacao cromossomica em 24 digitos.
	 * @param gene com digitos 0-7
	 * @return Cromossomo com 24 digitos binarios.
	 */
	public int[] converterCromossomo(int[] gene){
		int[] cromossomo = new int[tamVetor * tamCromossomo];
		int[] aux;
		
		//percorre os elementos do gene.
		for(int i = 0,j = 0; i < tamVetor;i++){
			//converte o valor pra bit.
			aux = this.deInteiroPraBit(gene[i]);
			//copia para dentro do vetor cromossomo.
			for(int x = 0; x < tamCromossomo;x++){
				cromossomo[j] = aux[x];
				j++;
			}
		}
		
		return cromossomo;
	}
	
	/**
	 * 	Metodo que transforma um cromossomo
	 *  em bits(24 digitos) em um gene (8 digitos) 
	 * @param cromossomo  em 28 digitos
	 * @return a representacao do tabuleiro em
	 *  numeros de 0-7
	 */
	public int[] desconverteCromossomo(int[] cromossomo){
		int[] gene = new int[tamVetor];
		int[] aux;
		//percorre as posicoes do gene a ser desconvertido.
		for(int i = 0,j = 0; i < tamVetor; i++){
			aux = new int[tamCromossomo];
			int cont = 0;
			//pega cada posicao do bit atual.
			do{
				aux[cont] = cromossomo[j];
				j++;
				cont++;
			}while(cont < 3);
			
			//insere no gene,o valor correto.
			gene[i] = this.deBitPraInteiro(aux);
		}
		
		return gene;
	}
	
	/**
	 * Metodo sem retorno que pega os dois primeiros
	 * individuos gerados na populacao e inverte
	 * os bits deles para gerar outros 2
	 * individuos com grande chance de ter 
	 * todos os cromossomos poss�veis.
	 */
	private void inverterBitPopInicial(){
		int[] aux;
		Individuo temp;
		Individuo novo;
		ArrayList<Individuo> listaTemp = new ArrayList<Individuo>();
		
		//cria dois filhos invertidos.
		for(int i = 0 ; i < 2;i++){
			temp = this.populacao.get(i);
			
			aux = this.converterCromossomo(temp.getTabuleiro());
			//inverte todos os bits do cromossomo inicial.
			for(int j = 0; j < (tamCromossomo * tamVetor); j++){
				if(aux[j] == 0){
					aux[j] = 1;
				}else{
					aux[j] = 0;
				}
			}
			
			novo = new Individuo(this.desconverteCromossomo(aux), null);
			listaTemp.add(novo);
		}
		this.populacao.addAll(listaTemp);
	}
	
	public ArrayList<Individuo> testeCriacaoPopulacaoInicial(){
		ArrayList<Individuo> retorno = new ArrayList<Individuo>();
		
		this.gerarPopulacaoInicial();
		
		retorno.addAll(this.populacao);
		
		return retorno;
	}
	
	
	public void testeCrossOver(){
		System.out.println("Populacao Inicial:");
		this.gerarPopulacaoInicial();
		this.imprimimePopulacao(this.populacao);
		this.gerarFilhos(this.populacao);
		System.out.println("===========================");
		System.out.println("Apos o Cross Over");
		this.imprimimePopulacao(this.populacao);
		
	}

}
