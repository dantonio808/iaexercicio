package segundaQuestao;

public class Amostra implements Comparable<Amostra> {
	private int classeObtida;
	private int classeEsperada;
	public double[] vet_caracteristicas;
	private double distancia;

	/* Construtor com a classe esperada e vetor de carateristicas. */
	public Amostra(int classeEsperada, double[] vet_caracteristicas) {
		super();
		this.classeEsperada = classeEsperada;
		this.vet_caracteristicas = vet_caracteristicas;
	}

	// Getters e Setters
	public int getClasseObtida() {
		return classeObtida;
	}

	public int getClasseEsperada() {
		return classeEsperada;
	}

	public double[] getVet_caracteristicas() {
		return vet_caracteristicas;
	}

	public void setClasseObtida(int classeObtida) {
		this.classeObtida = classeObtida;
	}

	public void setClasseEsperada(int classeEsperada) {
		this.classeEsperada = classeEsperada;
	}

	public void setVet_caracteristicas(double[] vet_caracteristicas) {
		this.vet_caracteristicas = vet_caracteristicas;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	@Override
	public int compareTo(Amostra o) {
		int valor = 0;
		
		if(o.getDistancia() == this.distancia){
			valor = 0;
		}else if(this.getDistancia() > o.getDistancia()){
			valor = 1;
		}else{
			valor = -1;
		}
		
		return valor;

	}

}
