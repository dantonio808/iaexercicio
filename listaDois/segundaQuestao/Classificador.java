package segundaQuestao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Classificador  {
	private int k;
	private int tipoDistancia;
	private ArrayList<Amostra> base;
	private ArrayList<Amostra> teste;
	private int qtdClasse1;
	private int qtdClasse2;
	private int qtdClasse3;
	private int acertos;
	
	public Classificador(int k, int tipoDistancia) {
		super();
		this.k = k;
		this.tipoDistancia = tipoDistancia;
		
		this.qtdClasse1 = 0;
		this.qtdClasse2 = this.qtdClasse1;
		this.qtdClasse3 = this.qtdClasse2;
		this.acertos = this.qtdClasse3;
		
		try {
			this.lerBaseDados();
			this.lerBaseTeste();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void lerBaseDados() throws FileNotFoundException{
		//String caminhoPc = "";
		String caminhoNTI = "//home//diego.ferreira//workspace//bitBucket//iaexercicio//dados//iris.txt";
		File arquivo = new File(caminhoNTI);
		Scanner scaner= new Scanner(arquivo);
		String aux;
		String[] valores;
		double[] vet;
		Amostra temp;
		this.base = new ArrayList<Amostra>();
		
		while(scaner.hasNext()){
			aux = scaner.next();
			
			valores = aux.split(",");
			vet = new double[valores.length];
			
			for(int i = 0;i < valores.length;i++){
				vet[i] = Double.parseDouble(valores[i]);
				
			}
			temp = new Amostra(Integer.parseInt(valores[valores.length -1]),vet);

			switch (temp.getClasseEsperada()) {
			case 1:
				this.qtdClasse1++;
				break;

			case 2:
				this.qtdClasse2++;
				break;

			case 3:
				this.qtdClasse3++;
				break;
			}
			this.base.add(temp);
		}
		
		scaner.close();
	}
	
	private void lerBaseTeste() throws FileNotFoundException{
		//String caminhoPc = "";
		String caminhoNTI = "//home//diego.ferreira//workspace//bitBucket//iaexercicio//dados//irisTeste.txt";
		File arquivo = new File(caminhoNTI);
		Scanner scaner= new Scanner(arquivo);
		String aux;
		String[] valores;
		double[] vet;
		Amostra temp;
		this.teste = new ArrayList<Amostra>();
		
		while(scaner.hasNext()){
			aux = scaner.next();
			
			valores = aux.split(",");
			vet = new double[valores.length];
			
			for(int i = 0;i < valores.length;i++){
				vet[i] = Double.parseDouble(valores[i]);
				
			}
			temp = new Amostra(Integer.parseInt(valores[valores.length -1]),vet);
			this.teste.add(temp);
		}
		
		scaner.close();
	}
	
	private double calculaDistanciaManhattan(Amostra atual,Amostra alvo){
		double distancia = 0.0;
		
		for(int i = 0; i < atual.vet_caracteristicas.length - 1; i++){
			distancia+= Math.abs(atual.vet_caracteristicas[i] - alvo.vet_caracteristicas[i]);
		}
		
		
		return distancia;
	}
	
	
	private void classificaAmostraAtual(Amostra atual){
		int classeEscolhida = this.contarClasses();
		
		if(classeEscolhida != -1){
			atual.setClasseObtida(classeEscolhida);
		}else{
			System.out.println("Nao se decidiu");
			atual.setClasseObtida(this.teste.get(0).getClasseEsperada());
		}
		
		
	}
	
	private int contarClasses(){
		int classe1 = 0;
		int classe2 = 0;
		int classe3 = 0;
		int classeFinal = -1;
		
		for(int i = 0; i < this.k;i++){
			
			switch(this.teste.get(i).getClasseEsperada()){
			case 1:
				classe1++;
			break;
			
			case 2:
				classe2++;
			break;
			
			case 3:
				classe3++;
			break;
			}
			
		}
		
		if(classe1 > classe2 && classe1 > classe3){
			classeFinal = 1;
		}
		
		if(classe2 > classe1 && classe2 > classe3){
			classeFinal = 2;
		}
		
		if(classe3 > classe1 && classe3 > classe2){
			classeFinal = 3;
		}
		
		return classeFinal;
	}
	
	private void avaliarAmostra(Amostra atual){
		if(atual.getClasseEsperada() == atual.getClasseObtida()){
			this.acertos++;
		}
	}
	
	
	private void saidaArquivo() throws FileNotFoundException{
//		String caminhoCasa = "";
		String caminhoNTI = "//home//diego.ferreira//Desktop";
		String nomeDefault = "saidaKNN.txt";
		PrintWriter escritor = new PrintWriter(caminhoNTI +"//" + nomeDefault);
		StringBuffer sb = new StringBuffer();
		
		for (Amostra atual : this.base) {
			avaliarAmostra(atual);
		}
		
		sb.append("Total Avaliados: " + this.base.size() + " \n");
		sb.append("Total de Acertos: " + this.acertos + "\n");
		
		escritor.write(sb.toString());
		
		escritor.close();
		
	}
	
	
	public void metodoKNN(){
		
		for (Amostra atual : this.base) {
			for(Amostra alvo : this.teste){
				alvo.setDistancia(this.calculaDistanciaManhattan(atual, alvo));
			}
			
			Collections.sort(this.teste);
			
			this.classificaAmostraAtual(atual);
		}
		
		
		try {
			this.saidaArquivo();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
